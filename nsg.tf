# resource "azurerm_network_security_rule" "kong" {
#   name                        = "Allow_kong"
#   priority                    = "200"
#   direction                   = "Inbound"
#   access                      = "Allow"
#   protocol                    = "*"
#   source_port_range           = "*"
#   source_address_prefixes     = ["190.153.235.117" # Moneda 970
#                                 ]
#   destination_port_ranges     = [8443]
#   destination_address_prefix  = "*"
#   resource_group_name         = "${module.qa.resource_group_name}"
#   network_security_group_name = "${module.qa.network_security_group_name}"
# }

resource "azurerm_network_security_rule" "kong" {
  name              = "Allow_kong"
  priority          = "200"
  direction         = "Inbound"
  access            = "Allow"
  protocol          = "*"
  source_port_range = "*"

  source_address_prefixes = ["190.153.235.117",
    "10.145.118.16/28",
    "10.145.112.128/28",
    "10.145.72.128/28",
    "10.145.14.192/28",
    "10.145.14.208/28",
  ]

  # Moneda 970, api-test, peinau-no-pci-test, peinau-no-pci-qa, cmr-cl-apertura-test, cmr-cl-apertura-qa
  destination_port_ranges     = [443, 8443]
  destination_address_prefix  = "*"
  resource_group_name         = "${module.swarm.resource_group_name}"
  network_security_group_name = "${module.swarm.network_security_group_name}"
}

resource "azurerm_network_security_rule" "rules_https" {
  name                        = "rules_443"
  priority                    = "200"
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  source_address_prefixes     = ["${var.nsg_allow_external_443}"]
  destination_port_ranges     = ["443", "8443"]
  destination_address_prefix  = "*"
  resource_group_name         = "${module.qa.resource_group_name}"
  network_security_group_name = "${module.qa.network_security_group_name}"
}
