provider "azurerm" {
  version = "1.44.0"
}

terraform {
  backend "azurerm" {
    container_name       = "tfstates"
    key                  = "banco-cl-cardless/qa/swarm_module.tfstate"
    resource_group_name  = "devops-ci"
    storage_account_name = "fiftfstatestorage"
  }

  required_version = ">= 0.11.10"
}

# vnet-fif module
module "vnet-fif-terraform" {
  source = "git::ssh://git@bitbucket.org/falabellafif/vnet-fif-terraform.git?ref=v1"
}
