variable "path_fif_validator" {
  type        = "string"
  description = "path_fif_validator"
  default     = ""
}

variable "environment" {
  type        = "string"
  description = "Environment"
  default     = ""
}

variable "slug" {
  type        = "string"
  description = "Service Name"
  default     = ""
}

variable "subnet_network" {
  type        = "string"
  description = "SubNet Network elasticsearch"
  default     = ""
}

variable "ip_public_name" {
  type    = "list"
  default = ["ip1", "ip2"]
}

variable "domain" {
  type    = "string"
  default = "fif.tech"
}

variable "nsg_allow_external_443" {
  type = "list"

  default = [
    "190.153.235.117", # Fif Office
    "200.10.170.90",   # Proxy Falabella
    "200.10.167.90",   # Proxy Falabella
  ]
}
